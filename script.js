var chart = c3.generate({
    bindto: '#chart',
	
    data: {
		xs: {
			cat: 'cat_x',
			dog: 'dog_x',
			rabbit: 'rabbit_x',
			ginipig: 'ginipig_x'
		},
		columns: [
			['cat', 600, 400, 30, 200, 300, 400, 700, 500, 450, 250],
			['cat_x', 20, 600, 500, 700, 200, 30, 400, 30, 200, 100],
			['dog', 600, 400, 30, 200, 300, 400, 700, 500, 450, 250],
			['dog_x', 700, 200, 30, 400, 30, 200, 100, 400, 150, 250],
			['rabbit', 30, 180, 300, 30, 200, 700, 100, 400, 100, 30],
			['rabbit_x', 100, 300, 400, 700, 30, 20, 30, 100, 30, 100],
			['ginipig', 100, 300, 400, 700, 30, 20, 30, 100, 30, 100],
			['ginipig_x', 30, 180, 300, 30, 200, 700, 100, 400, 100, 30]
		],
		type: 'scatter'
    },
	
	axis: {
		x: {
			label: {
			    text: 'Probability of Failure',
			    position: 'outer-center'
			},
			min: 0,
            max: 800
		},
		y: {
			label: {
			    text: 'Consequence of Failure',
			    position: 'outer-center'
			}
		}
    },
	point: {
		r: 5
	},
	regions: [
		{axis: 'x', start: 400, class:'probability'},
		{axis: 'y', start: 300, class:'consequence'}
	]
});

var button_load_unload = document.getElementById("button-load-unload");
function load_unload(){
	
	if(button_load_unload.innerHTML=="Load"){
		chart.load({
			xs: {
				tortoise: 'tortoise_x',
				parrot: 'parrot_x'
			},
			columns: [
				['tortoise', 80, 20, 10, 40, 10, 500, 20, 70, 10, 40],
				['tortoise_x', 300, 200, 10, 40, 10, 50, 20, 700, 100, 400],
				['parrot', 500, 20, 100, 40, 15, 650, 600, 400, 900, 20],
				['parrot_x', 50, 20, 10, 40, 15, 650, 600, 40, 90, 200]
			],
			type: 'line'
		});
		
		button_load_unload.innerHTML = "Unload";
		button_load_unload.classList.remove("btn-success");
		button_load_unload.classList.add("btn-danger");
	}else{
		chart.unload({
			ids: ['tortoise', 'parrot']
		});
		
		button_load_unload.innerHTML = "Load";
		button_load_unload.classList.remove("btn-danger");
		button_load_unload.classList.add("btn-success");
	}
}

var button_show_hide = document.getElementById("button-show-hide");
function show_hide(){
	
	if(button_show_hide.innerHTML=="Show"){
		chart.show(['dog', 'rabbit']);
		
		button_show_hide.innerHTML = "Hide";
		button_show_hide.classList.remove("btn-success");
		button_show_hide.classList.add("btn-danger");
	}else{
		chart.hide(['dog', 'rabbit']);
		
		button_show_hide.innerHTML = "Show";
		button_show_hide.classList.remove("btn-danger");
		button_show_hide.classList.add("btn-success");
	}
}

var button_show_hide_axis = document.getElementById("button-show-hide-axis");
function show_hide_axis(){
	if(button_show_hide_axis.innerHTML=="Show Axis"){
		chart.xgrids([
			{value: 400, text:''}
		]);
		chart.ygrids([
			{value: 300, text:''}
		]);
		
		button_show_hide_axis.innerHTML = "Do not Show Axis";
		button_show_hide_axis.classList.remove("btn-success");
		button_show_hide_axis.classList.add("btn-danger");
	}else{
		chart.xgrids.remove(
			{value: 400}
		);
        chart.ygrids.remove(
			{value: 300}
		);
		
		button_show_hide_axis.innerHTML = "Show Axis";
		button_show_hide_axis.classList.remove("btn-danger");
		button_show_hide_axis.classList.add("btn-success");
	}
}

